from .run import run

BENCHMARKS = [
    "furmark",
#    "gimark",
    "piano",
    "volplosion",
    "plot3d",
    "tessmark",
    "triangle",
]

import math
import os
import sixonix
import sys

# TODO: These already exists in the "statistics" package in Python 3.4.
# We should just use those once we're willing to accept the dependency.
def mean(data):
    s = 0
    n = 0
    for x in data:
        s += x
        n += 1
    return s / n

def stdev(data):
    x_ = mean(data)
    return math.sqrt(mean(map(lambda x: (x - x_)**2, data)))

def run_benchmark(args):
    import sixonix

    if args.benchmark not in sixonix.BENCHMARKS:
        print("ERROR: unknown benchmark.  Choose one of:")
        for test in sixonix.BENCHMARKS:
            print("    " + test)
        sys.exit(-1)

    env = os.environ
    if args.mesa_path is not None:
        env = env.copy()
        sixonix.mesa.MesaPrefix(args.mesa_path).update_env(env)

    if args.average:
        fps = [sixonix.run(args.benchmark, args, env)
               for _ in range(args.average)]
        print('{} ± {}'.format(mean(fps), stdev(fps)))
    else:
        fps = sixonix.run(args.benchmark, args, env)
        print(fps)

def register_cmd(subparsers):
    parser = subparsers.add_parser('run', help='run a single benchmark',
                                   parents=[sixonix.run_argparse])
    parser.add_argument('benchmark', help="benchmark to run")
    parser.add_argument('--mesa-path', type=str, default=None,
                        help='path to the mesa build to use')
    parser.add_argument('--average', metavar='N', type=int, default=None,
                        help='Run the benchmark N times and average them')
    parser.set_defaults(func=run_benchmark)

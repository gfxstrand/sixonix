#!/usr/bin/env python3

"""Installs all the benchmarks"""

import sixonix

def install_benchmarks(args):
    if args.suite:
        for name in args.suite:
            sixonix.SUITES[name].install()
    else:
        for name, module in sixonix.SUITES.items():
            module.install()

def register_cmd(subparsers):
    parser = subparsers.add_parser('install',
        help='download and install benchmark binaries')
    parser.add_argument('suite', nargs='*', type=str,
                        help="benchmark suite to install")
    parser.set_defaults(func=install_benchmarks)
